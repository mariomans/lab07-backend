package se331.lab.rest.entitiy;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Student {
    long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    Integer penAmount;
    String description;

    public void setId(long id) {
        this.id = id;
    }
}
