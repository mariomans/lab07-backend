package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entitiy.Student;


import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("https://vignette.wikia.nocookie.net/riverdalearchie/images/2/22/Season_2_%27Diner%27_Archie_Andrews_Promotional_Portrait.png/revision/latest?cb=20171204210022")
                .penAmount(15)
                .description("The great man ever!!!")
                .build());
        this.students.add(Student.builder()
                .id(2)
                .studentId("SE-002")
                .name("Cherprang")
                .surname("BNK48")
                .gpa(4.01)
                .image("https://vignette.wikia.nocookie.net/riverdalearchie/images/1/15/Season_2_%27Diner%27_Betty_Cooper_Promotional_Portrait.jpg/revision/20171207043729")
                .penAmount(2)
                .description("COde for Thailand")
                .build());
        this.students.add(Student.builder()
                .id(3)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("http://cdn3-www.superherohype.com/assets/uploads/gallery/riverdale-1x01/rvd01_fo_cheryl_0001.jpg")
                .penAmount(0)
                .description("Welcome to Olypic")
                .build());
    }

    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return  ResponseEntity.ok(students);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return  ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity saveStundent(@RequestBody Student student){
        student.setId((long) this.students.size()+1);
        this.students.add(student);
        return  ResponseEntity.ok(student);
    }
}